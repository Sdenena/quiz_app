import React from 'react';
import { createStackNavigator,createAppContainer} from 'react-navigation';
import Home from './Home';
import Results from './Results';

  const AppNavigator = createStackNavigator({    
    Home: Home,
    Results: Results
        
 }
  );
  export default createAppContainer(AppNavigator);