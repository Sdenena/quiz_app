import React, { Component } from 'react';
import {StyleSheet,View,Image,TouchableOpacity} from 'react-native'
import {RadioGroup,RadioButton} from 'react-native-flexi-radio-button'

import {Container, Content, Card, Text} from 'native-base'

export default class Home extends Component {
  static navigationOptions = {
    title: 'Home',
  };

   constructor(){
        super()
        this.state=({
            answer : [],  
            score:0,         
            questions: [
                {
                    "text": "What is the name of the US president?", 
                    "correct": 1, 
                    "answers": [
                        "Obama", 
                        "Trump", 
                        "Roosvelt", 
                        "Putin"
                    ]
                }, 
                {
                    "text": "What is the square root of 100?", 
                    "correct": 3, 
                    "answers": [
                        "sin(10)", 
                        "1", 
                        "100", 
                        "10"
                    ]
                },
                {
                  "text": "What is the JavaScript?", 
                  "correct": 1, 
                  "answers": [
                      "Backend framework", 
                      "Font-End framework", 
                      "Serverside", 
                      "Database"
                  ]
              },
              {
                "text": "What is the HTML?", 
                "correct": 0, 
                "answers": [
                    "Markup language", 
                    "Scripting language", 
                    "Web Server", 
                    "StyleSheets"
                ]
            }
            ] ,                     
             
        })
    }
    handleCheck=(question,answer,correct)=>{
        this.props.navigation.navigate('Results',this.state.score)
    }


    selectHandle = (index,i) =>{
      let selected=this.state.answer     

      selected[i]=index

      this.setState({
        answer:selected
      })
      console.log(selected);
      let newScore=0;
      for(let i in this.state.answer){
       
        if(this.state.questions[i].correct == this.state.answer[i]){
          newScore++
          var numOfQuestion=this.state.questions.length
          if(newScore === numOfQuestion){
            this.setState({
              score:newScore/numOfQuestion*100
            })
          }else{
            this.setState({
              score:((newScore/numOfQuestion*100).toFixed(2))
            })
            
          }
        }       
      }    
    }

   


  render() {
      
    return (
      <Container>
        <Content padder>
          <Text style={styles.title}>Please choose the answer</Text>
          <Card style={styles.card}>
             {
                this.state.questions.map((question,i) => (
                <View key={i}  style={styles.container}>                     
                  <Text style={styles.question}>{question.text}</Text>
                    <RadioGroup
                        size={24}
                        hickness={2}
                        onSelect = {(index) => this.selectHandle(index, i)}
                    >
                     {question.answers.map(answer => (
                                  
                        <RadioButton key={answer} value={answer} color='blue'>
                          <Text>{answer}</Text>
                        </RadioButton>                                      
                     ))}
                 </RadioGroup>
                 </View>
              ))
             }
             
         </Card>
           
         <TouchableOpacity onPress={this.handleCheck} style={styles.buttonStyle}>
           <Image
             style={styles.img}
            source={{ uri: 'http://chittagongit.com//images/next-icon/next-icon-11.jpg' }}
           />
         </TouchableOpacity>
            

      </Content>
      </Container>
    );
  }
}

let styles = StyleSheet.create({
  container: {
      flexDirection: 'column'
  },
  text: {
      padding: 10,
      fontSize: 14,
  },
  buttonStyle: {
      marginTop: 10,
      alignSelf: 'center'
  },
  img:{
      width: 90,
      height: 90,
      marginBottom: 20,
  },
  card:{
    marginBottom:10
  },
  title:{
    marginBottom:10,
    marginTop:10,
    alignSelf:'center'
  },
  question:{
    marginLeft:5,
    marginTop:5
  }
})
