import React, { Component } from 'react';
import { View, Text,StyleSheet,Image } from 'react-native';

export default class Results extends Component {
  static navigationOptions = {
    title: 'Result',
  };
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    console.log(this.props);
    return (
      <View style={{flex:1,justifyContent:"center",alignItems:'center'}}>
          {this.props.navigation.state.params==100?(<Image
             style={styles.img}
            source={{ uri: 'https://czechtongue.cz/wp-content/uploads/2017/09/medaile-nebo-medajle.png' }}/>
           ):(
             <Text style={styles.info}>Try more</Text>
           )}
         
        <Text style={styles.text}> Your score is: {this.props.navigation.state.params}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flexDirection: 'column'
  },
  text: {
      padding: 10,
      fontSize: 20,
  },
  img:{
      width: 90,
      height: 90,
      marginBottom: 20,
  },
  info:{
    fontSize:20,
    color:'yellow'
  }
})